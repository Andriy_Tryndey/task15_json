package com.andriy.model;

public class GrowingTips {
    private int temperature;
    private boolean likesLighting;
    private double watering;

    public GrowingTips() {
    }

    public GrowingTips(int temperature, boolean likesLighting, double watering) {
        this.temperature = temperature;
        this.likesLighting = likesLighting;
        this.watering = watering;
    }

    public int getTemperature() {
        return temperature;
    }

    public GrowingTips setTemperature(int temperature) {
        this.temperature = temperature;
        return this;
    }

    public boolean isLikesLighting() {
        return likesLighting;
    }

    public GrowingTips setLikesLighting(boolean likesLighting) {
        this.likesLighting = likesLighting;
        return this;
    }

    public double getWatering() {
        return watering;
    }

    public GrowingTips setWatering(double watering) {
        this.watering = watering;
        return this;
    }
}

