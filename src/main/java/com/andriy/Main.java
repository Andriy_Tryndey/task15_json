package com.andriy;

import com.andriy.json.JSONValidator;
import com.andriy.json.gson.JSONGsonParser;
import com.andriy.json.jackson.JSONJacksonParser;
import com.andriy.model.Flower;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        File json = new File("src/main/resources/flowers.json");
        File schema = new File("src/main/resources/flowersSchema.json");

        try {
            JSONValidator.validate(json, schema);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ProcessingException e) {
            e.printStackTrace();
        }

        JSONJacksonParser parser = new JSONJacksonParser();
        JSONGsonParser gsonParser = new JSONGsonParser();

        System.out.println("JacksonParser");
        printList(parser.getFlowerList(json));
        System.out.println("GsonParser");
        printList(gsonParser.getFlowerList(json));
    }
    private static void printList(List<Flower> flowers){
        for (Flower flower : flowers){
            System.out.println(flower);
        }
    }
}
