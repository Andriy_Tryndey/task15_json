package com.andriy.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;

import java.io.File;
import java.io.IOException;

public class JSONValidator {
    public static boolean validate(File jsonPath, File schemaPath) throws IOException, ProcessingException {
        JsonNode data = JsonLoader.fromFile(jsonPath);
        JsonNode schema = JsonLoader.fromFile(schemaPath);

        JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        JsonValidator validator = factory.getValidator();
        ProcessingReport report = validator.validate(schema, data);
        return report.isSuccess();
    }
}
