package com.andriy.json.jackson;

import com.andriy.model.Flower;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JSONJacksonParser {
    private ObjectMapper objectMapper;

    public JSONJacksonParser() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Flower> getFlowerList(File jsonFile) {
        Flower [] flowers = new Flower[0];
        try {
            flowers = objectMapper.readValue(jsonFile, Flower[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Arrays.asList(flowers);
    }
}
