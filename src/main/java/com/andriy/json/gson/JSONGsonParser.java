package com.andriy.json.gson;

import com.andriy.model.Flower;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class JSONGsonParser {
    private Gson gson;
    public JSONGsonParser(){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }
    public List<Flower> getFlowerList(File json){
        Flower [] flowers = new Flower[0];
        try {
            flowers = gson.fromJson(new FileReader(json), Flower[].class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return Arrays.asList(flowers);
    }
    public void writeToFile(List<Flower> flowersList, File json) {
        try (Writer writer = new FileWriter(json)){
            gson.toJson(flowersList, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
